<?php

declare(strict_types=1);

/* by creating a class by DB type (dev, test, prod), 
* accesses can be easily changed  
*/
include_once './classes/dbDev.php';
include_once './classes/dbCrud.php';

// create an instance of devDb connection
$myDb = new DbDev;
$myDb = $myDb->devConnect();
var_dump($myDb);


// create an instance of CRUD class
$myCRUD = new DbCRUD;

// SELECT
$query = $myCRUD->select($myDb, 'todos',array('id','label','done'));
var_dump($query);

// INSERT


// UPDATE

// DELETE
// $query = $myCRUD->delete($myDb, 'todos', 17);

// TEST IF TABLES EXISTS
// $query = $myCRUD->tableExists($myDb, 'todos');

// var_dump($query);

