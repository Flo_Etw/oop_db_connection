<?php
// a strict type is declared for each variable
declare(strict_types=1);

// abstract class
require_once dirname(__FILE__) . '/abstractDbConnection.php';

// subClass MySQL
class MysqlConnection extends AbstractConnection
{

    // properties
    private const DB_DRIVER = 'mysql';

    // setters
    public function setAddress($address): bool
    {
        $this->db_address = $address;
        // todo : implement tests / validations
        if ('' === $address) {
            $this->valid_db_address = false;
        } else {
            $this->valid_db_address = true;
        }
        return $this->valid_db_address;
    }

    public function setDbName($dbName): bool
    {
        $this->db_name = $dbName;
        // todo : implement tests / validations
        if ('' === $dbName) {
            $this->valid_db_name = false;
        } else {
            $this->valid_db_name = true;
        }
        return $this->valid_db_name;
    }

    public function setUser($user): bool
    {
        $this->db_user = $user;
        // todo : implement tests / validations
        if ('' === $user) {
            $this->valid_db_user = false;
        } else {
            $this->valid_db_user = true;
        }
        return $this->valid_db_user;
    }

    public function setPassword($pw): bool
    {
        $this->db_password = $pw;
        // todo : implement tests / validations
        if ('' === $pw) {
            $this->valid_db_password = false;
        } else {
            $this->valid_db_password = true;
        }
        return $this->valid_db_password;
    }


    /**
     * connect to mysql driver
     * 
     * @return object if connection succeeds, return PDO / Else return array with empty/invalid params
     * @example 
     * 
     */
    public function dbConnect(): object
    {
        
        $inValidAttributes = $this->findEmptyAttribs();

        if (!empty($inValidAttributes)) {
            $result = (object) $inValidAttributes;
        } else {
            $result = $this->connectionSkeleton(self::DB_DRIVER . ':host=' . $this->getAddress() .';dbname=' 
            . $this->getDbName(), $this->getUser(), $this->getPassword());
        }
        return $result;
    }

    /**
     * check if all mandatory connection attributes are filled 
     * @return array containing setters methods of empty attributes
     * - add here all new connection attributes
     */
    private function findEmptyAttribs(): array
    {
        $inValidAttributes = array();
        if (!$this->valid_db_name) {
            array_push($inValidAttributes, array("setDbName"));
        }
        if (!$this->valid_db_user) {
            array_push($inValidAttributes, array("setUser"));
        }
        if (!$this->valid_db_password) {
            array_push($inValidAttributes, array("setPassword"));
        }
        if (!$this->valid_db_address) {
            array_push($inValidAttributes, array("setAddress"));
        }
        return $inValidAttributes;
    }
}