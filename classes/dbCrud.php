<?php

declare(strict_types=1);

class DbCRUD {

    // properties

    // methods

    // select request
    public function select(object $pdo, string $tableName, array $arrayParams)
    {
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $arrayFields = implode(", ", $arrayParams);
        $sql = "SELECT $arrayFields FROM $tableName";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        return $result;
    }


    // il faut vérifier que l'id existe bien et renvoyer une erreur si suppression d'id non existant
    // utiliser if exists ?
    public function delete(object $pdo, string $tableName, int $id) 
    { 
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "DELETE FROM $tableName WHERE id = $id";
        $stmt = $pdo->prepare($sql);
        $stmt = $pdo->query($sql);
        
        // todo : gérer l'erreur
        // if($stmt->fetchColumn()) return $pdo->errorInfo();
        // var_dump($pdo->errorInfo());
        

        if ($stmt == false) {
            echo 'Error: cannot delete id ' . $id . ' from table ' . $tableName;
            return false;
        } else {
            return true;
        }
    }


    /**
     * Check if a table exists in the current database.
     *
     * @param PDO $pdo PDO instance connected to a database.
     * @param string $table Table to search for.
     * @return bool TRUE if table exists, FALSE if no table found.
     */
    function tableExists($pdo, $tableName) {

        // Try a select statement against the table
        // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
        try {
            $result = $pdo->query("SELECT 1 FROM $tableName LIMIT 1");
        } catch (Exception $e) {
            // We got an exception == table not found
            return 'FALSE';
        }

        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return $result !== FALSE;
    }

    // faire test sur les users (droit d'exécution)

}