<?php
// a strict type is declared for each variable
declare(strict_types=1);

abstract class AbstractConnection
{

    // properties
    // todo : rename to less obvious names
    protected $db_name = '';
    protected $db_user = '';
    protected $db_password = '';
    protected $db_address = '';

    // control attributes, if they're valid set = true
    protected $valid_db_name = false;
    protected $valid_db_user = false;
    protected $valid_db_password = false;
    protected $valid_db_address = false;
    // protected $validAttributes = array(
    //     "setDbName" => FALSE, "setUser" => FALSE,
    //     "setPassword" => FALSE, "setAddress" => FALSE
    // );

    // methods

    // implemented in each subClass created for each driver
    public abstract function dbConnect();

    // setters : will be impleted in each subClass / return a boolean
    public abstract function setDbName($dbName): bool;
    public abstract function setUser($user): bool;
    public abstract function setPassword($pw): bool;
    public abstract function setAddress($address): bool;

    // getters
    protected function getDbName()
    {
        return $this->db_name;
    }

    protected function getUser()
    {
        return $this->db_user;
    }

    protected function getPassword()
    {
        return $this->db_password;
    }

    protected function getAddress()
    {
        return $this->db_address;
    }

    // common structure for all the subClasses
    protected function connectionSkeleton($dsn, $user, $password): object
    {

        try {
            $result = new PDO($dsn, $user, $password);

            // Driver configuration: to see exceptions
            $result->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (Exception $e) {
            // todo : to be implemented with customized messages according to the main error code families
            $result = (object) ['code' => $e->getMessage(), 'message' => 'Probleme de connexion'];
        } finally {
            return $result;
        }
    }
}

?>