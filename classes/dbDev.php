<?php 

    include_once dirname(__FILE__)  . '/abstractDbConnection.php';
    include_once dirname(__FILE__)  . '/mySqlPdoConnection.php';

    class DbDev{
        
        // private $con;

        function devConnect(){
            include_once dirname(__FILE__)  . '/secret/connectInfos.php';

            $db = new MysqlConnection(); 
            $db->setAddress(DB_HOST);
            $db->setUser(DB_USER);
            $db->setPassword(DB_PASSWORD);
            $db->setDbName(DB_NAME);

            // var_dump($db);
            return $db->dbConnect();
        }
        
    }